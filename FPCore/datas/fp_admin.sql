/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 80019
Source Host           : localhost:3306
Source Database       : fp_admin

Target Server Type    : MYSQL
Target Server Version : 80019
File Encoding         : 65001

Date: 2021-11-19 15:52:48
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for fp_appinfo
-- ----------------------------
DROP TABLE IF EXISTS `fp_appinfo`;
CREATE TABLE `fp_appinfo` (
  `id` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'ID',
  `name` varchar(200) DEFAULT NULL,
  `apppath` varchar(255) DEFAULT NULL,
  `author` varchar(100) DEFAULT NULL,
  `icon` varchar(100) DEFAULT NULL,
  `appkey` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `intro` text,
  `version` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `target` varchar(50) DEFAULT NULL,
  `adminindex` varchar(255) DEFAULT NULL,
  `apptype` int DEFAULT NULL COMMENT '是否系统应用',
  `datetime` datetime DEFAULT NULL,
  `updatetime` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of fp_appinfo
-- ----------------------------
INSERT INTO `fp_appinfo` VALUES ('14819286672786432', '方配后台管理系统', '/admin/', '方配', 'fpadmin.png', 'FPAdmin', '提供方配.Core后台管理系统，官方网站：http://www.fangpage.com', '1.0.0', '', '', '1', '2021-04-18 16:00:14', '2021-10-25 14:18:04');
INSERT INTO `fp_appinfo` VALUES ('14819288422794240', '系统用户管理', '/user/', '方配', '', 'FPUser', '提供方配.Core后台用户管理，官方网站：http://www.fangpage.com', '1.0.0', '', '', '1', '2021-04-19 13:07:58', '2021-06-08 09:24:36');
INSERT INTO `fp_appinfo` VALUES ('15322740856259584', '验证码插件', '/plugins/verify/', '方配', '', 'FPVerify', '提供在应用中使用验证码功能。', '1.0.0', '', 'verify', '2', '2021-06-08 08:58:03', '2021-06-08 10:57:58');
INSERT INTO `fp_appinfo` VALUES ('15345878991946752', 'Froala富文本编辑器', '/plugins/froala_editor/', 'Froala', '', 'FroalaEditor', '富文本编辑器', '1.0.0', '', 'https://froala.com/wysiwyg-editor', '2', '2021-06-24 17:15:23', '2021-06-25 13:53:29');
INSERT INTO `fp_appinfo` VALUES ('15346034237195264', 'Font Awesome', '/plugins/font-awesome/', 'FontAwesome', '', 'FontAwesome', '一套绝佳的图标字体库和CSS框架', '4.7.0', '', 'https://fontawesome.dashgame.com', '2', '2021-06-24 19:53:19', '2021-06-25 12:05:37');
INSERT INTO `fp_appinfo` VALUES ('15346050112472064', 'Animate', '/plugins/animate/', 'Animate', '', 'Animate', '强大的跨平台的预设css3动画库', '4.1.1', '', 'http://www.animate.net.cn', '2', '2021-06-24 20:09:27', '2021-06-25 13:52:42');

-- ----------------------------
-- Table structure for fp_department
-- ----------------------------
DROP TABLE IF EXISTS `fp_department`;
CREATE TABLE `fp_department` (
  `id` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `parentid` varchar(100) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `shortname` varchar(255) DEFAULT NULL,
  `depth` int DEFAULT NULL,
  `display` int DEFAULT NULL,
  `status` int DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of fp_department
-- ----------------------------
INSERT INTO `fp_department` VALUES ('14819286672786432', '', '研发部', '', '0', '1', '0');
INSERT INTO `fp_department` VALUES ('14819288422794240', '14819286672786432', '开发部', '', '0', '1', '0');
INSERT INTO `fp_department` VALUES ('14819288578327552', '14819286672786432', '测试部', '', '0', '2', '0');

-- ----------------------------
-- Table structure for fp_menuinfo
-- ----------------------------
DROP TABLE IF EXISTS `fp_menuinfo`;
CREATE TABLE `fp_menuinfo` (
  `id` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `parentid` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `name` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `markup` varchar(100) DEFAULT NULL,
  `icon` varchar(255) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `depth` int DEFAULT NULL,
  `display` int DEFAULT NULL,
  `status` int DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of fp_menuinfo
-- ----------------------------
INSERT INTO `fp_menuinfo` VALUES ('14816339144819712', '15555233462535168', '主页', 'homeindex', 'layui-icon layui-icon-home', 'sys_config.html', '0', '1', '1');
INSERT INTO `fp_menuinfo` VALUES ('14817755636073472', '15555233462535168', '系统设置', '', 'layui-icon layui-icon-set', '', '0', '2', '1');
INSERT INTO `fp_menuinfo` VALUES ('14817838928593920', '15555233462535168', '用户设置', '', 'layui-icon layui-icon-user', '', '0', '3', '1');
INSERT INTO `fp_menuinfo` VALUES ('14817847193945088', '14817755636073472', '菜单管理', null, '', '/admin/menu_list.html', '0', '3', '1');
INSERT INTO `fp_menuinfo` VALUES ('14817878472803328', '14817838928593920', '角色管理', null, '', '/user/role_list.html', '0', '2', '1');
INSERT INTO `fp_menuinfo` VALUES ('14819214596637696', '14817838928593920', '部门管理', null, '', '/user/depart_list.html', '0', '1', '1');
INSERT INTO `fp_menuinfo` VALUES ('14819291942732800', '14817838928593920', '用户管理', null, '', '/user/user_list.html', '0', '4', '1');
INSERT INTO `fp_menuinfo` VALUES ('14821765541135360', '14817755636073472', '应用管理', null, '', '/admin/app_manage.html', '0', '2', '1');
INSERT INTO `fp_menuinfo` VALUES ('14829225151841281', '14817755636073472', '通用设置', null, '', '/admin/sys_config.html', '0', '1', '1');
INSERT INTO `fp_menuinfo` VALUES ('15555233462535168', '', '系统管理', '', 'layui-icon layui-icon-set', '', '0', '1', '1');

-- ----------------------------
-- Table structure for fp_roleinfo
-- ----------------------------
DROP TABLE IF EXISTS `fp_roleinfo`;
CREATE TABLE `fp_roleinfo` (
  `id` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `code` varchar(255) DEFAULT NULL,
  `detail` varchar(255) DEFAULT NULL,
  `isadmin` varchar(255) DEFAULT NULL,
  `departs` varchar(255) DEFAULT NULL,
  `menus` varchar(255) DEFAULT NULL,
  `status` int DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of fp_roleinfo
-- ----------------------------
INSERT INTO `fp_roleinfo` VALUES ('14819199099962368', '超级管理员', 'super', '', '1', '14819286672786432,14819288578327552', '15555233462535168,14816339144819712,14817755636073472,14829225151841281,14821765541135360,14817847193945088,14817838928593920,14819214596637696,14817878472803328,14819291942732800,15555259537310720,15555271218414592', '1');

-- ----------------------------
-- Table structure for fp_userinfo
-- ----------------------------
DROP TABLE IF EXISTS `fp_userinfo`;
CREATE TABLE `fp_userinfo` (
  `id` varchar(60) NOT NULL,
  `username` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `headimg` varchar(255) DEFAULT NULL,
  `realname` varchar(255) DEFAULT NULL,
  `gender` varchar(255) DEFAULT NULL,
  `mobile` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `pid` varchar(255) DEFAULT NULL,
  `roleid` varchar(255) DEFAULT NULL,
  `departid` varchar(255) DEFAULT NULL,
  `openid` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `content` varchar(255) DEFAULT NULL,
  `status` int DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of fp_userinfo
-- ----------------------------
INSERT INTO `fp_userinfo` VALUES ('14820260937352192', 'admin', 'e10adc3949ba59abbe56e057f20f883e', null, '管理员', '男', '13481092810', 'fangpage@foxmail.com', 'q', '14819199099962368', '14819288422794240', '0', '', '1');
